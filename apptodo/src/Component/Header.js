import React from "react";
import { Layout, Menu, Breadcrumb } from 'antd';

const { Header} = Layout;

const Headerpage = () => {
    return (
        <div>
            <di>
                <Layout>
                    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['1']}
                            style={{ lineHeight: '64px' }}
                        >
                            <Menu.Item key="1" >
                                <a target="_blank" rel="noopener noreferrer" href="/" >Home</a></Menu.Item>
                            <Menu.Item key="2"><a target="_blank" rel="noopener noreferrer">Todo</a></Menu.Item>
                            <Menu.Item key="3"><a target="_blank" rel="noopener noreferrer">Albums</a></Menu.Item>
                        </Menu>
                    </Header>
                </Layout>
            </di>
        </div>
    );
}

export default Headerpage;
