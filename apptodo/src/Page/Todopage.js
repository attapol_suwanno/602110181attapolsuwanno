import React, { useState, useEffect } from 'react'
import { Descriptions, Breadcrumb, PageHeader } from 'antd';
import { Menu, Layout, Dropdown, Button, Icon, List, Typography } from 'antd';
import Headerpage from '../Component/Header';
import Footerpage from '../Component/Footer'


const { Content } = Layout;

const TodoPage = (props) => {
    const [user, setUser] = useState([])
    const [todolist, setTodolist] = useState([])
    const [completTest, setcompletTest] = useState('All')

    const fechUserData = () => {
        const userId = props.match.params.user_id;

        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
    }

    const fetchtodoList = () => {
        const userId = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodolist(data);
            })
    }

    useEffect(() => {
        fechUserData();
        fetchtodoList();

    }, [])

    const onChangebutton = (item) => {
        let datachangButton = [...todolist]
        datachangButton[item.id-1].completed = true;
        setTodolist(datachangButton)
       
    }

    const handleMenuClick = (datasave) => {
        setcompletTest(datasave.key)
    }

    const menu = (todolist) => {
        let completFilter = [];
        todolist.map(datacomplet => {

            if (!completFilter.includes(datacomplet.completed))
                completFilter.push(datacomplet.completed)
        });
        return (
            <Menu onClick={handleMenuClick}>
                {
                    completFilter.map((list) => {

                        return (
                            <Menu.Item value={list} key={list}>
                                <Icon />
                                {
                                    list == true ?
                                        "Done"
                                        :
                                        "Doing"
                                }
                            </Menu.Item>
                        )
                    })}
            </Menu>
        )
    }
    return (

        <div>
            <Layout>
                <div>
                    <Headerpage />
                </div>
                <Content style={{ padding: '0 50px', marginTop: 64 }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Todo</Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                        style={{ border: '1px solid rgb(235, 237, 240)', }}
                        onBack={()=>window.history.back()}
                        title="Home"
                    />
                    <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>

                        <Descriptions title={user.name} >
                            <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                            <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                        </Descriptions>
                        <div id="components-dropdown-demo-dropdown-button">
                            <Dropdown overlay={menu(todolist)}>
                                <Button>   
                                    {completTest=='All' ?
                                    ('All')
                                    :
                                     completTest == true ?
                                        ("Done")
                                        :
                                        ("Doing")
                                }
                                </Button>
                            </Dropdown>
                        </div>
                        <List
                            bordered
                            dataSource={
                                completTest == 'All' ?
                                    todolist
                                    :
                                    todolist.filter((data) => {
                                        return (data.completed + "" == completTest + "")
                                    })
                            }
                            renderItem={(item,index) => (
                                <div>
                                    {<List.Item>
                                        {
                                            item.completed == true ?
                                                <Typography.Text mark>Done</Typography.Text>
                                                :
                                                <Typography.Text delete>Doing</Typography.Text>
                                        }
                                        {item.title}
                                        {
                                            item.completed == true ?
                                                <Button className='buttonDo' >
                                                    Done
                                                </Button>
                                                :
                                                <Button className='buttonDo' type="primary" onClick={() => onChangebutton(item)} >
                                                    Doing
                                                </Button>
                                        }
                                    </List.Item>
                                    }
                                </div>
                            )}
                        />
                    </div>
                </Content>
                <div>
                    <Footerpage />
                </div>
            </Layout>

        </div>
    )
}
export default TodoPage;