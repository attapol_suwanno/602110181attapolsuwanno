import React, { useState, useEffect } from "react";
import "../App.css";
import { Layout, Breadcrumb, PageHeader, List, Avatar } from 'antd';
import Headerpage from '../Component/Header';
import Footerpage from '../Component/Footer'
const { Content } = Layout;

const Albumspage = (props) => {
    const [albumsData, setAlbumData] = useState([])

    const fechListdata = () => {
        const userId = props.match.params.user_id;

        fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbumData(data);
            })
            .catch(eror => console.log(eror))
    }

    useEffect(() => {
        fechListdata();
    }, [])


    return (
        <div>
            <Layout>
                <div>
                    <Headerpage />
                </div>
                <Content style={{ padding: '0 50px', marginTop: 64 }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item >Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Album</Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                        style={{ border: '1px solid rgb(235, 237, 240)', }}
                        onBack={()=>window.history.back()}
                        title="Home"
                    />
                    <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
                        <List
                            itemLayout="horizontal"
                            dataSource={albumsData}
                            renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title={<a href={'/album/' + item.id + '/photo'}>{item.title}</a>}
                                    />
                                </List.Item>
                            )}
                        />
                    </div>
                </Content>
                <div>
                    <Footerpage />
                </div>
            </Layout>
        </div>
    )
}
export default Albumspage;