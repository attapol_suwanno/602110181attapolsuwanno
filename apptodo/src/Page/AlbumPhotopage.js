import React, { useState, useEffect } from "react";
import "../App.css";
import { List, Card, Layout, Breadcrumb, PageHeader,BackTop} from 'antd';
import Headerpage from '../Component/Header';
import Footerpage from '../Component/Footer'

const { Content } = Layout;
const { Meta } = Card;

const AlbumPhotopage = (props) => {
    const [albumPhoto, setalbumPhoto] = useState([])

    const fechListphoto = () => {
        const albumId = props.match.params.album_id;

        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
            .then(response => response.json())
            .then(data => {
                setalbumPhoto(data);
            })
            .catch(eror => console.log(eror))
    }

    useEffect(() => {
        fechListphoto();
    }, [])

    return (
        <div>
            <Layout>
                <div>
                    <Headerpage />
                </div>
                <Content style={{ padding: '0 50px', marginTop: 64 }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Album</Breadcrumb.Item>
                        <Breadcrumb.Item>AlbumList</Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                        style={{ border: '1px solid rgb(235, 237, 240)', }}
                        onBack={() => window.history.back()}
                        title="Album"

                    />
                    <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>


                        <List grid={{ gutter: 2, column: 4 }}
                            dataSource={albumPhoto}
                            renderItem={item => (
                                <List.Item>
                                    <Card hoverable style={{ width: 200 }} cover={<img src={item.url} />} >
                                        <Meta title={item.title} description={item.thumbnailUrl} />
                                    </Card>
                                </List.Item>
                            )}
                        />
                    </div>
                </Content>
                <div>
                    <Footerpage />
                </div>
                <div>
                    <BackTop />
                    Scroll down to see the bottom-right
                    <strong style={{ color: 'rgba(64, 64, 64, 0.6)' }}> gray </strong>
                    button.
                </div>
            </Layout>
        </div>
    )
}
export default AlbumPhotopage;